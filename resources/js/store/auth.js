import axios from 'axios'
import router from '../router'
const state = {
  authenticated: false,
  user: [],
  errors: [],
  loading: false,
  message: null
};

const getters = {
  authenticated (state) {
    return state.authenticated
  },
  user (state) {
    return state.user
  },
  errors (state) {
    return state.errors
  },
  loading (state) {
    return state.loading
  },
  message (state) {
    return state.message
  },
};

const mutations = {
  SET_AUTHENTICATED (state, value) {
    state.authenticated = value
  },
  SET_USER (state, value) {
    state.user = value
  },
  SET_ERROR (state, value) {
    state.errors = value
  },
  SET_MESSAGE (state, value) {
    state.message = value
  },
  TOGGLE_LOADING (state) {
    state.loading = !state.loading
  }
};

const actions = {
  async signIn ({ dispatch, commit, state }, credentials) {
    commit('TOGGLE_LOADING')
    await axios.get('/csrf-cookie')
    await axios.post('/login', credentials)
    .then(response => {
      window.localStorage.setItem("token", response.data.token);
      axios.get('/profile').then((response) => {
        commit('SET_AUTHENTICATED', true)
        commit('SET_USER', response.data)
        router.go({name: 'dashboard'});
        
      }).catch(() => {
        commit('SET_AUTHENTICATED', false)
        commit('SET_USER', null)
      })
    })
    .catch(({ response }) => {
      commit('SET_ERROR', response.data)
    });
    commit('TOGGLE_LOADING')
  },

  async signOut ({ dispatch }) {
    await axios.post('/logout')
    router.go({name: 'login'});
  },

  async me ({ commit }) {
    return await axios.get('/profile').then((response) => {
      commit('SET_AUTHENTICATED', true)
      commit('SET_USER', response.data)
      router.go({name: 'dashboard'});
      
    }).catch(() => {
      commit('SET_AUTHENTICATED', false)
      commit('SET_USER', null)
    })
  },
  async register ({ commit }, credentials) {
    commit('TOGGLE_LOADING')
    await axios.get('/csrf-cookie')
    await axios.post('/register', credentials)
    .then(response => {
      commit('SET_USER', response.data)
      commit('SET_MESSAGE', response.message)
    })
    .catch(({ response }) => {
      console.log(response);
      commit('SET_ERROR', response.data.errors)
    });
    commit('TOGGLE_LOADING')
  },
};


export default {
  state,
  actions,
  mutations,
  getters
};