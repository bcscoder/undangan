import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

import LoginComponent from './components/LoginComponent';
import RegisterComponent from './components/RegisterComponent';
import DashboardComponent from './components/DashboardComponent'
import ThemesComponent from './components/ThemesComponent'
import UserComponent from './components/UserComponent'
import OrderComponent from './components/OrderComponent'

const router = new VueRouter({
    mode: 'history',
    routes: [
        {
            path: '/register',
            name: 'register',
            component: RegisterComponent
        },
        {
            path: '/login',
            name: 'login',
            component: LoginComponent
        },
        {
            path: '/dashboard',
            name: 'dashboard',
            component: DashboardComponent
        },
        {
            path: '/themes',
            name: 'themes',
            component: ThemesComponent
        },
        {
            path: '/users',
            name: 'users',
            component: UserComponent
        },
        {
            path: '/orders',
            name: 'orders',
            component: OrderComponent
        }
    ]
});

export default router;
