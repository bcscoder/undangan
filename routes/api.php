<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
Route::prefix('v1')->group(function () {
    Route::post('/login','LoginController@store');
    Route::post('/register','RegisterController@create');
    Route::post('/logout','LoginController@logout');

    Route::middleware('auth:sanctum')->group(function () {
        Route::get('/profile','ProfileController@index');
        Route::resource('/themes','ThemeController');
    });
});

