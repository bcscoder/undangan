<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Theme extends Model
{
    use HasFactory;

    protected $fillable = [
        'type',
        'name',
        'description',
        'image'
    ];

    public function sections(){
        return $this->hasMany(ThemeSection::class);
    }
    
}
