<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationGallery extends Model
{
    use HasFactory;

    protected $fillable = [
        'name',
        'invitation_id',
        'description',
        'filename',
        'order',
    ];
}
