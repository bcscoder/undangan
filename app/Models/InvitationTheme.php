<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class InvitationTheme extends Model
{
    use HasFactory;

    protected $fillable = [
        'invitation_id',
        'theme_id',
        'section_id',
        'title',
        'description',
    ];
}
